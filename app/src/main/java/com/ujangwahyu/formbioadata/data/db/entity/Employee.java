package com.ujangwahyu.formbioadata.data.db.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.ujangwahyu.formbioadata.utils.DateConverter;

import java.util.Date;

@Entity(tableName = "employee")
@TypeConverters(DateConverter.class)
public class Employee {

    @PrimaryKey(autoGenerate = true)
    public long id;
    public String name;
    public String department;
    public String position;
    public Date join;
    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    public byte[] image;

    @Ignore
    public Employee() {
        this.name = "";
        this.department = "";
        this.position = "";
        this.join = null;
        this.image = null;
    }

    public Employee(String name, String department, String position, Date join, byte[] image) {
        this.name = name;
        this.department = department;
        this.position = position;
        this.join = join;
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Date getJoin() {
        return join;
    }

    public void setJoin(Date join) {
        this.join = join;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}