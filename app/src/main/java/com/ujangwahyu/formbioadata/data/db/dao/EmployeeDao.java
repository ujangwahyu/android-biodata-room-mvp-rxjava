package com.ujangwahyu.formbioadata.data.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.ujangwahyu.formbioadata.data.db.entity.Employee;

import java.util.List;

import static androidx.room.OnConflictStrategy.IGNORE;

@Dao
public interface EmployeeDao {

    @Query("SELECT * FROM employee ORDER BY name ASC")
    LiveData<List<Employee>> findAllPersons();

    @Query("SELECT * FROM employee")
    List<Employee> getAllChannels();

    @Query("SELECT * FROM employee WHERE id=:id")
    Employee findPersonById(String id);

    @Query("SELECT * FROM employee WHERE id=:id")
    Employee findPerson(long id);

    @Insert(onConflict = IGNORE)
    long insertPerson(Employee employee);

    @Update
    int updatePerson(Employee employee);

    @Update
    void updatePerson(List<Employee> employee);

    @Delete
    void deletePerson(Employee employee);

    @Query("DELETE FROM employee")
    void deleteAll();
}
