package com.ujangwahyu.formbioadata.module.listemployee;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.ujangwahyu.formbioadata.data.db.dao.EmployeeDao;
import com.ujangwahyu.formbioadata.data.db.entity.Employee;

import java.util.List;

public class ListPresenter implements ListContract.Presenter {

    private final ListContract.View mView;
    private final EmployeeDao employeeDao;

    public ListPresenter(ListContract.View view, EmployeeDao employeeDao) {
        this.mView = view;
        this.mView.setPresenter(this);
        this.employeeDao = employeeDao;
    }

    @Override
    public void start() {

    }

    @Override
    public void addNewPerson() {
        mView.showAddEmployee();
    }

    @Override
    public void result(int requestCode, int resultCode) {

    }

    @Override
    public void populateEmployee() {
        employeeDao.findAllPersons().observeForever(new Observer<List<Employee>>() {
            @Override
            public void onChanged(@Nullable List<Employee> employees) {
                mView.setEmployee(employees);
                if (employees == null || employees.size() < 1) {
                    mView.showEmptyMessage();
                }
            }
        });
    }

    @Override
    public void openEditScreen(Employee employee) {
        mView.showEditScreen(employee.id);
    }

    @Override
    public void openConfirmDeleteDialog(Employee employee) {
        mView.showDeleteConfirmDialog(employee);
    }

    @Override
    public void delete(long employeeId) {
        Employee employee = employeeDao.findPerson(employeeId);
        employeeDao.deletePerson(employee);
    }
}
