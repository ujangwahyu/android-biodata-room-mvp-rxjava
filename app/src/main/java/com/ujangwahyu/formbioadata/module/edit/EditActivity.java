package com.ujangwahyu.formbioadata.module.edit;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.ujangwahyu.formbioadata.R;
import com.ujangwahyu.formbioadata.data.AppDatabase;
import com.ujangwahyu.formbioadata.data.db.entity.Employee;
import com.ujangwahyu.formbioadata.fragment.DateDialogFragment;
import com.ujangwahyu.formbioadata.utils.Constants;
import com.ujangwahyu.formbioadata.utils.ImageConverter;
import com.ujangwahyu.formbioadata.utils.Util;

import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditActivity extends AppCompatActivity implements EditContract.View, EditContract.DateListener {

    private EditContract.Presenter mPresenter;

    private EditText etName;
    private EditText etDepartment;
    private EditText etPosition;
    private EditText etJoin;

    private TextInputLayout tlName;
    private TextInputLayout tlDepartment;
    private TextInputLayout tlPosition;
    private TextInputLayout tlJoin;

    private Button btnImage;
    private CircleImageView imgEmployee;
    private Bitmap bitmap;

    private FloatingActionButton fabSave;

    private Employee employee;
    private boolean mEditMode = false;

    final int CAMERA_INTENT = 51;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        employee = new Employee();
        checkMode();

        AppDatabase db = AppDatabase.getDatabase(getApplication());
        mPresenter = new EditPresenter(this, db.employeeDao());

        initViews();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mEditMode) {
            mPresenter.getEmployeeAndPopulate(employee.id);
        }
    }

    private void checkMode() {
        if (getIntent().getExtras() != null) {
            employee.id = getIntent().getLongExtra(Constants.EMPLOYEE_ID, 0);
            mEditMode = true;
        }
    }


    private void initViews() {
        etName = findViewById(R.id.et_name);
        etDepartment = findViewById(R.id.et_department);
        etPosition = findViewById(R.id.et_position);
        etJoin = findViewById(R.id.et_join);

        tlName = findViewById(R.id.tl_name);
        tlDepartment = findViewById(R.id.tl_department);
        tlPosition = findViewById(R.id.tl_position);
        tlJoin = findViewById(R.id.tl_join);

        btnImage = findViewById(R.id.btn_ambil_gambar);
        imgEmployee = findViewById(R.id.img_employee);
        bitmap = null;

        etJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.showDateDialog();
            }
        });

        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.showImage();
            }
        });

        fabSave = findViewById(R.id.fab);
        fabSave.setImageResource(mEditMode ? R.drawable.ic_refresh_black_24dp : R.drawable.ic_save_black_24dp);
        fabSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                employee.name = etName.getText().toString();
                employee.department = etDepartment.getText().toString();
                employee.position = etPosition.getText().toString();
                employee.image = ImageConverter.converterImage2ByteArray(bitmap);

                boolean valid = mPresenter.validate(employee);

                if (!valid) return;

                if (mEditMode) {
                    mPresenter.update(employee);
                } else {
                    mPresenter.save(employee);
                }
            }
        });
    }
    @Override
    public void showErrorMessage(int field) {
        if (field == Constants.FIELD_NAME) {
            tlName.setError(getString(R.string.invalid_name));
        } else if (field == Constants.FIELD_DEPARTMENT) {
            tlDepartment.setError(getString(R.string.invalid_department));
        } else if (field == Constants.FIELD_POSITION) {
            tlPosition.setError(getString(R.string.invalid_position));
        } else if (field == Constants.FIELD_JOIN) {
            tlJoin.setError(getString(R.string.invalid_join));
        }
    }

    @Override
    public void clearPreErrors() {
        tlName.setErrorEnabled(false);
        tlDepartment.setErrorEnabled(false);
        tlPosition.setErrorEnabled(false);
        tlJoin.setErrorEnabled(false);
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void populate(Employee employee) {
        this.employee = employee;

        etName.setText(employee.name);
        etDepartment.setText(employee.department);
        etPosition.setText(employee.position);
        etJoin.setText(Util.format(employee.join));
        imgEmployee.setImageBitmap(ImageConverter.converByteArray2Image(employee.image));
    }

    @Override
    public void openDateDialog() {
        DateDialogFragment fragment = new DateDialogFragment();
        fragment.show(getSupportFragmentManager(), "datePicker");
    }

    private boolean check_ReadStoragepermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this.getApplicationContext(),
                        Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

            return true;
        } else {
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    //requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA},
                            Constants.permission_Read_data);
                }
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
        }
        return false;
    }

    @Override
    public void openImage() {
        if(check_ReadStoragepermission()){
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if(intent.resolveActivity(getPackageManager()) != null){
                startActivityForResult(intent, CAMERA_INTENT);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case CAMERA_INTENT:
                bitmap = (Bitmap) data.getExtras().get("data");
                if (bitmap != null){
                    imgEmployee.setImageBitmap(bitmap);
                }
                break;
        }
    }

    @Override
    public void setPresenter(EditContract.Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void setSelectedDate(Date date) {
        employee.join = date;
        etJoin.setText(Util.format(date));
    }
}
