package com.ujangwahyu.formbioadata.module.listemployee;

import com.ujangwahyu.formbioadata.base.BasePresenter;
import com.ujangwahyu.formbioadata.base.BaseView;
import com.ujangwahyu.formbioadata.data.db.entity.Employee;

import java.util.List;

public interface ListContract {

    interface Presenter extends BasePresenter {

        void addNewPerson();

        void result(int requestCode, int resultCode);

        void populateEmployee();

        void openEditScreen(Employee employee);

        void openConfirmDeleteDialog(Employee employee);

        void delete(long personId);
    }

    interface View extends BaseView<Presenter> {

        void showAddEmployee();

        void setEmployee(List<Employee> employee);

        void showEditScreen(long id);

        void showDeleteConfirmDialog(Employee employee);

        void showEmptyMessage();
    }

    interface OnItemClickListener {

        void clickItem(Employee employee);

        void clickLongItem(Employee employee);
    }

    interface DeleteListener {

        void setConfirm(boolean confirm, long personId);

    }
}
