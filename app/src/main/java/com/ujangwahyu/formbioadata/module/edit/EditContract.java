package com.ujangwahyu.formbioadata.module.edit;

import com.ujangwahyu.formbioadata.base.BasePresenter;
import com.ujangwahyu.formbioadata.base.BaseView;
import com.ujangwahyu.formbioadata.data.db.entity.Employee;

import java.util.Date;

public interface EditContract {

    interface Presenter extends BasePresenter {
        void save(Employee employee);

        boolean validate(Employee employee);

        void getEmployeeAndPopulate(long id);

        void update(Employee employee);

        void showDateDialog();

        void showImage();
    }

    interface View extends BaseView<Presenter> {

        void showErrorMessage(int field);

        void clearPreErrors();

        void close();

        void populate(Employee employee);

        void openDateDialog();

        void openImage();
    }

    interface DateListener {

        void setSelectedDate(Date date);

    }
}
