package com.ujangwahyu.formbioadata.module.listemployee;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ujangwahyu.formbioadata.R;
import com.ujangwahyu.formbioadata.adapter.EmployeeAdapter;
import com.ujangwahyu.formbioadata.data.AppDatabase;
import com.ujangwahyu.formbioadata.data.db.entity.Employee;
import com.ujangwahyu.formbioadata.fragment.DeleteConfirmFragment;
import com.ujangwahyu.formbioadata.module.edit.EditActivity;
import com.ujangwahyu.formbioadata.utils.Constants;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.TextView;

import java.util.List;

public class EmployeeActivity extends AppCompatActivity implements ListContract.View, ListContract.OnItemClickListener, ListContract.DeleteListener {

    private ListContract.Presenter mPresenter;
    private EmployeeAdapter employeeAdapter;

    private TextView tvEmpty;
    private RecyclerView recyclerView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.addNewPerson();
            }
        });

        tvEmpty = findViewById(R.id.tv_empty);
        recyclerView = findViewById(R.id.list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        employeeAdapter = new EmployeeAdapter(this);
        recyclerView.setAdapter(employeeAdapter);

        AppDatabase db = AppDatabase.getDatabase(getApplication());
        mPresenter = new ListPresenter(this, db.employeeDao());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.populateEmployee();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void showAddEmployee() {
        Intent intent = new Intent(this, EditActivity.class);
        startActivity(intent);
    }

    @Override
    public void setEmployee(List<Employee> employee) {
        tvEmpty.setVisibility(View.GONE);
        employeeAdapter.setValues(employee);
    }

    @Override
    public void showEditScreen(long id) {
        Intent intent = new Intent(this, EditActivity.class);
        intent.putExtra(Constants.EMPLOYEE_ID, id);
        startActivity(intent);
    }

    @Override
    public void showDeleteConfirmDialog(Employee employee) {
        DeleteConfirmFragment fragment = new DeleteConfirmFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(Constants.EMPLOYEE_ID, employee.id);
        fragment.setArguments(bundle);
        fragment.show(getSupportFragmentManager(), "confirmDialog");
    }

    @Override
    public void showEmptyMessage() {
        tvEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void clickItem(Employee employee) {
        mPresenter.openEditScreen(employee);
    }

    @Override
    public void clickLongItem(Employee employee) {
        mPresenter.openConfirmDeleteDialog(employee);
    }

    @Override
    public void setConfirm(boolean confirm, long personId) {
        if (confirm) {
            mPresenter.delete(personId);
            finish();
            getIntent().addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(getIntent());
            overridePendingTransition(0, 0);
        }
    }

    @Override
    public void setPresenter(ListContract.Presenter presenter) {
        mPresenter = presenter;
    }
}
