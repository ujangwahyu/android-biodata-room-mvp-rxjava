package com.ujangwahyu.formbioadata.module.edit;

import com.ujangwahyu.formbioadata.data.db.dao.EmployeeDao;
import com.ujangwahyu.formbioadata.data.db.entity.Employee;
import com.ujangwahyu.formbioadata.utils.Constants;
import com.ujangwahyu.formbioadata.utils.Util;

public class EditPresenter implements EditContract.Presenter {

    private final EditContract.View mView;
    private final EmployeeDao employeeDao;

    public EditPresenter(EditContract.View mMainView, EmployeeDao employeeDao) {
        this.mView = mMainView;
        this.mView.setPresenter(this);
        this.employeeDao = employeeDao;
    }

    @Override
    public void start() {

    }

    @Override
    public void save(Employee employee) {
        long ids = this.employeeDao.insertPerson(employee);
        mView.close();
    }

    @Override
    public boolean validate(Employee employee) {
        mView.clearPreErrors();
        if (employee.name.isEmpty() || !Util.isValidName(employee.name)) {
            mView.showErrorMessage(Constants.FIELD_NAME);
            return false;
        }

        if (employee.name.isEmpty() || !Util.isValidDepartment(employee.department)) {
            mView.showErrorMessage(Constants.FIELD_DEPARTMENT);
            return false;
        }

        if (employee.name.isEmpty() || !Util.isValidPosition(employee.position)) {
            mView.showErrorMessage(Constants.FIELD_POSITION);
            return false;
        }

        return true;
    }

    @Override
    public void getEmployeeAndPopulate(long id) {
        Employee person = employeeDao.findPerson(id);
        if (person != null) {
            mView.populate(person);
        }
    }

    @Override
    public void update(Employee employee) {
        int ids = this.employeeDao.updatePerson(employee);
        mView.close();
    }

    @Override
    public void showDateDialog() {
        mView.openDateDialog();
    }

    @Override
    public void showImage() {
        mView.openImage();
    }
}
