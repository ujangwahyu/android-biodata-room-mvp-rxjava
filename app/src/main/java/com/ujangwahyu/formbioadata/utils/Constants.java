package com.ujangwahyu.formbioadata.utils;

public class Constants {

    public static final int FIELD_NAME = 10;
    public static final int FIELD_DEPARTMENT = 13;
    public static final int FIELD_POSITION = 14;
    public static final int FIELD_JOIN = 14;

    public static int permission_camera_code = 786;
    public static int permission_write_data = 788;
    public static int permission_Read_data = 789;
    public static int permission_Recording_audio = 790;

    public static final String EMPLOYEE_ID = "employee_id";
}
