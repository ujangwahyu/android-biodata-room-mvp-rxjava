package com.ujangwahyu.formbioadata.base;

public interface BaseView<T> {

    void setPresenter(T presenter);

}
