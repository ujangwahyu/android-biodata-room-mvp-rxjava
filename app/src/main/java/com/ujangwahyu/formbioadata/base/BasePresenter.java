package com.ujangwahyu.formbioadata.base;

public interface BasePresenter {

    void start();

}
