package com.ujangwahyu.formbioadata.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ujangwahyu.formbioadata.R;
import com.ujangwahyu.formbioadata.data.db.entity.Employee;
import com.ujangwahyu.formbioadata.module.listemployee.ListContract;
import com.ujangwahyu.formbioadata.utils.ImageConverter;
import com.ujangwahyu.formbioadata.utils.Util;

import java.util.ArrayList;
import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.ViewHolder> {

    private List<Employee> mValues;
    private ListContract.OnItemClickListener mOnItemClickListener;

    public EmployeeAdapter(ListContract.OnItemClickListener onItemClickListener) {
        mValues = new ArrayList<>();
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_person, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.tvName.setText(mValues.get(position).name);
        holder.tvDepartment.setText(mValues.get(position).department);
        holder.tvPosition.setText(holder.mItem.position);
        holder.tvJoin.setText(Util.formatMin(holder.mItem.join));
        if(holder.mItem.image != null){
            holder.imgEmployee.setImageBitmap(ImageConverter.converByteArray2Image(holder.mItem.image));
        }

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.clickItem(holder.mItem);
            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.clickLongItem(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void setValues(List<Employee> values) {
        mValues = values;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvName;
        public final TextView tvDepartment;
        public final TextView tvPosition;
        public final ImageView imgEdit;
        public final ImageView imgDelete;
        public final TextView tvJoin;
        public final ImageView imgEmployee;
        public Employee mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvName = view.findViewById(R.id.tv_name);
            tvDepartment = view.findViewById(R.id.tv_department);
            tvPosition = view.findViewById(R.id.tv_position);
            imgEdit = view.findViewById(R.id.img_edit);
            imgDelete = view.findViewById(R.id.img_delete);
            tvJoin = view.findViewById(R.id.tv_join);
            imgEmployee = view.findViewById(R.id.img_employee);
        }
    }


}
